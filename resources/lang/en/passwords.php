<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Contraseña debe tener como mínimo 6 caracteres y debe coincidir con su confirmación.',
    'reset' => 'Su contraseña ha sido restaurada.',
    'sent' => 'Hemos enviado un link de restauración a su correo electrónico.',
    'token' => 'El token de restauración de contraseña no e válido.',
    'user' => "No encontramos un usuario con ese correo electrónico.",

];
